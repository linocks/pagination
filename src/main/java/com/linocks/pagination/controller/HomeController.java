package com.linocks.pagination.controller;

import com.linocks.pagination.repository.StaffRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController{

    @Autowired
    StaffRepository staffRepo;

    @GetMapping("/")
    public String getHome(Model model,@RequestParam(defaultValue="0") String page){

        model.addAttribute("staffList", staffRepo.findAll(PageRequest.of(Integer.parseInt(page), 5)));
        model.addAttribute("currentPage", page);
        return "index";
    }
}