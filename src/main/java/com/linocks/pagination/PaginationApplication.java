package com.linocks.pagination;

import com.linocks.pagination.entity.Staff;
import com.linocks.pagination.repository.StaffRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaginationApplication implements CommandLineRunner{
	@Autowired
	StaffRepository staffRepo;

	public static void main(String[] args) {
		SpringApplication.run(PaginationApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		for(int i=0; i<30; i++){
			staffRepo.save(new Staff("firstname "+(i+1), "lastname "+(i+1), "dept "+(i+1), 4000+(i+1)));
		}
	}

}
