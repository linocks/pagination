package com.linocks.pagination.repository;

import com.linocks.pagination.entity.Staff;

import org.springframework.data.jpa.repository.JpaRepository;

public interface StaffRepository extends JpaRepository<Staff,Long>{
    
}